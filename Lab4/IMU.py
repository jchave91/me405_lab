## @file IMU.py
#  The IMU.py is created to encapsulate some methods required to take advantage of the.
#
#  The IMU.py creates a class called IMU. Within this class you have the constructor that initializes the created bus in which the IMU will interact with the nucleo board.
#  The class also contains 8 methods that will provide functions to interact with the IMU. You will be able to enable or disable the IMU by setting the appropriate power
#  level or setting the entire system into sleep mode. You will be able to set the mode to NDOF, which uses all three sensors. The user will be able to get the mode that
#  the IMU is in by calling the method get_mode. There is a method that retrieves the current calibration of the IMU. It displays the current calibration of the accelerometer,
#  gyroscope, and the magnetometer. Once calibrated, you will be able to retrieve data from any of the three sensors with the remaining methods. The user can retrieve accleration data
#  by calling the get_accel method, angular acceleration data by calling the get_gyro method, and the Euler angles by calling the get_eul method.
#    
#  @author Jose Chavez
#
#  @date May 30, 2020
## Latest Update
#  @date June 2, 2020

import utime
import pyb
from pyb import I2C
import ustruct
from micropython import const

NDOF = const(0x0C)          #NDOF mode activates all 3 sensors
PWR = const(0x3E)           #Power mode- address within the slave
suspend = const(2)          #Pauses the system
BNO055 = const(0x28)        #Slave address
OPR = const(0x3D)           #OPR_MODE- address within the slave
ACC_DATA = const(0x08)      #Lower byte of X axis Acceleration data
GYR_DATA = const(0x14)      #Lower byte of X axis Gyroscope data
MAG_DATA = const(0x0E)      #Lower byte of X axis Magnetometer data
EUL_DATA = const(0x1A)      #Lower byte of heading data
Calib_Stat = const(0x35)    #Calibration status register
Convertion = const(100)     # Used to convert acceleration data to m/s^2 (1 m/s^2 = 100 LSB AND 1 mg = 1 LSB) 
class IMU:
    #Initialize the IMU
    def __init__(self, i2c):
        '''Initializing the I2C object to access all the attributes in the IMU class.
        @param i2c Is the object created for bus one. The SDA and SCL pins are on this bus.'''
        self.i2c = i2c
        
    ##Wakeup
    #
    #
    def  enable (self):
        ''' This method will enable the BNO055 by placing the power mode into normal. If the IMU was put into sleep
        mode, this method will essentially wake it up.'''
        i2c.mem_write(PWR, BNO055, 0) # Places the system in normal mode. All sensors are always switched ON in this mode.
        
    ##Sleep
    #
    #
    def disable (self):
        '''This method will disable the BNO055 sensors by placing the entire system into sleep mode.'''
        
        i2c.mem_write(PWR, BNO055, 2) #Pauses the system and places all sensors and microcontroller into sleep mode        
    
    ##Set the IMU mode
    #
    # 
    def set_mode(self):
        ''' This method can set the BNO055 mode to whatever the user wants it to be. For now, it only sets the
        mode to NDOF, will accesses all three sensors.'''
        
        #Sets the operation mode to NDOF (This step fully enables the imu)
        i2c.mem_write(NDOF, BNO055, OPR)
        
    #Provide the user with the current mode the IMU is in
    def get_mode(self):
        '''This method is with the user in mind, This method is called upon when the user would like to know what
        mode the BNO055 is in.'''
        
        # Read the operation mode register
        data = i2c.mem_read(1, BNO055, OPR)
        self.mode = ustruct.unpack('<b', data)[0]    # Extract the first value from the tuple
        # check the register number returned and print the mode that register number corresponds to
        if (self.mode == 12):
            print('The BNO055 is in NDOF mode')
        else:
            pass

    def get_accel(self):
        ''' This function reads the acceleration values off of the sensor. The values are converted into readable numbers
        then into the units desired, which are m/s^2'''
        data = self.i2c.mem_read(8, BNO055, ACC_DATA)  #Read 8 bytes representing the LSB and MSB of the acceleration sensor
        x, y, z = ustruct.unpack('<hhh', data)    #Unpack the data into a 3-tuple [units are in mg]
        #Divide by the convertion factor to get the data in m/s^2
        x = x/Convertion
        y = y/Convertion
        z = z/Convertion
        return (x,y,z)
    
    def get_gyro(self):
        ''' This function reads the gyroscope sensor data.'''
        data = self.i2c.mem_read(8, BNO055, GYR_DATA)
        x, y, z = ustruct.unpack('<hhh', data)
        return (x,y,z)
    
    def get_eul(self):
        ''' This function reads the Euler angles.'''
        data = self.i2c.mem_read(8, BNO055, EUL_DATA)
        #Unpack the data into a 3 tuple
        x, y, z = ustruct.unpack('<hhh', data)
        #Convert the 3 tuple into degrees by dividing by 16 (1 deg = 16 LSB)
        x = x/16
        y = y/16
        z = z/16
        return (x,y,z)
    
    #Provide the IMU's current calibration status
    def get_calibration(self):
        ''' This function will retrieve the calibration register which is a single byte of data. Each pair of bits within
        this register represents the calibration status for each sensor. The main purpose of this function is to isolate
        each pair and display them. The user will determine which pair to display depending on their interest. The pin
        descriptions are as follows: Pins <7:6> correspond to the system's calibration status, pins <5:4> correspond to the
        gyroscope's calibration status, pins <3:2> correspond to the acceleration's calibration status, and pins <1:0> correspond
        to the magnetometer's calibration status. \n
        All status display a range between 3 for fully calibrated and 0 for not calibrated. '''
        
        #Retrieve the calibration register located at 0x35
        data = self.i2c.mem_read(NDOF, BNO055, Calib_Stat)
        
        #Shift the data from the calibration register to the right as needed, then mask the result in order to only display the
        #bits desired.
        
        #Magnetometer calibration status
        self.mag_cal = (data[0] >> 0) & 0b11
        
        #Acceleration calibration status
        self.acc_cal = (data[0] >> 2) & 0b11
        
        #This will shift the register bits 4 and 5 to the right four times in order to get them into bits 0 and 1 (right alinged).
        #The result is then masked with 00000011 in order to clear all bits except the first two.
        
        #Gyroscope calibration status
        self.gyr_cal = (data[0] >> 4) & 0b11
        
        #Return the calibration value for each sensor
        return self.mag_cal, self.acc_cal, self.gyr_cal
    

if __name__ =='__main__':
    # Any code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    bus = 1
    i2c = I2C(bus, I2C.MASTER)  #Create the I2C on bus 1 and init as a master  
    imu = IMU(i2c)              #Creating the IMU object and passing the I2C bus in to the constructor
    
    #Set the IMU mode to NDOF
    imu.set_mode()
    #Simple for loop that runs once every second for 100 times.
    #This is just to test the Euler angle outputs.
    run = input('Run? (y)es or (n)o: ')
    while (True):
        if (run == 'y'):
            for n in range(100):
                print('{}'.format(imu.get_eul()))
                utime.sleep_ms(1000)
        elif (run == 'n'):
            print('End')
            break
    run = input('Run again? (y)es or (n)o: ')

