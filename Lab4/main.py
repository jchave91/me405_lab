## @file main.py
#  Brief doc for main.py
#
#  Detailed doc for main.py 
#
#  @author Jose Chavez
#
#  @copyright License Info
#
#  @date January 1, 1970
#
#
import pyb
import utime
import array
from Motor_Driver import MotorDriver
from En_coder import Encoder
from MotorControl import CLPropCtrl
import IMU


if __name__ =='__main__':
    # Any code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    ## Motor Driver Initialization
    #    

    # Create the pin objects used for interfacing with the motor driver   
    # Sets pins of interest as output pins
    EN_pin  = pyb.Pin.cpu.A10
    pin_IN1 = pyb.Pin.cpu.B4
    pin_IN2 = pyb.Pin.cpu.B5
    
    # Create the pin objects used for interfacing with motor2   
    # Sets pins of interest as output pins
    EN_pinB  = pyb.Pin.cpu.C1
    pin_IN1B = pyb.Pin.cpu.A0
    pin_IN2B = pyb.Pin.cpu.A1    
    
    # Create the timer object used for PWM generation
    timer = pyb.Timer(3, freq = 20000);
    timer2 = pyb.Timer(5, freq = 20000);
    
    # Create a motor object passing in the pins and timer
    moe1 = MotorDriver(EN_pin, pin_IN1, pin_IN2, timer)
    moe2 = MotorDriver(EN_pinB, pin_IN1B, pin_IN2B, timer2)
    
    #  Enable the motor driver
    moe1.enable()
    moe2.enable()

    #  Set the duty cycle to 0 so motor will not spin
    moe1.set_duty(0)
    moe2.set_duty(0)
    
    ## Encoder Initialization
    #
    #
    # Sets the pins for encoder 1 as input pins
    ENCA1 = pyb.Pin (pyb.Pin.cpu.B6, pyb.Pin.IN)
    ENCB1 = pyb.Pin (pyb.Pin.cpu.B7, pyb.Pin.IN)
    
    # Sets the pins for encoder 2 as input pins
    ENCA2 = pyb.Pin (pyb.Pin.cpu.C6, pyb.Pin.IN)
    ENCB2 = pyb.Pin (pyb.Pin.cpu.C7, pyb.Pin.IN)
    
    # Encoder 1 works on timer 4
    Timer1 = 4
    
    # Encoder 2 works on timer 8
    Timer2 = 8
    
    # The period is preset to 65535
    period = 65535
    
    # The prescaler is set to 0
    prescaler = 0
    
    # Encoder naming shortcut
    encoder1 = Encoder(Timer1, ENCA1, ENCB1, prescaler, period) 
    encoder2 = Encoder(Timer2, ENCA2, ENCB2, prescaler, period)
    
    ## Motor Proportion Control Initialization
    Kp1 = 0.075    
    MoControl1 = CLPropCtrl( Kp1)
    
    Kp2 = 0.075    
    MoControl2 = CLPropCtrl( Kp2)
    
    ## Toggle Switch
    #
    # Set Pin A7 as an Input pin
    Switch = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.IN)

    ## DoNothing's Main Program
    Theta_Meas1 = 0
    delta_sum1 = 0
    Theta_Meas2 = 0
    delta_sum2 = 0    
    while (True):           
        
        # Check the state of the switch
        State = Switch.value()
        
        #  Condition for switch in the ON position
        if (State == 1):
            #  Opening the Door
            for i in range (0, 100):
                #  Enable the motor driver
                moe2.enable()
                print('yes')
                # Updates the encoder2 with the most recent position and calculates a new delta
                encoder2.update()
                    
                # Store the current value of delta as the new delta and add it to the running total of the deltas.
                new_delta2 = encoder2.position
                delta_sum2 += new_delta2
                    
                # Save the running total of the deltas as the distance traveled so far (Theta_Meas).
                Theta_Meas2 = delta_sum2   
            
                # Update the proportional motor control 2, with the most recent measured theta and the setpoint
                Theta_Ref2 = 100
                    
                MoControl2.update(Theta_Meas2, Theta_Ref2)
                # Using the newly calculated actuation_signal from the encoder update
                # set the duty cycle
                actuation2 = MoControl2.Actuation_Signal
                moe2.set_duty(actuation2)

                #  This is a delay that will allow the recording of the values to 
                #  be more gradual not performing this may cause the program to 
                #  crash due to the large amount of iterations it must perform. 
                utime.sleep_ms(10)
            
            else:
                #  When the for loop is done, this will disable the motor and 
                #  proceed
                print('no')
                moe2.disable()
                
            #  Moving the Lever Arm UP
            for i in range (0, 100):
                #  Enable the motor driver
                moe1.enable()
                
                # Updates the encoder1 with the most recent position and calculates a new delta
                encoder1.update()
                    
                # Store the current value of delta as the new delta and add it to the running total of the deltas.
                new_delta1 = encoder1.position
                delta_sum1 += new_delta1
                    
                # Save the running total of the deltas as the distance traveled so far (Theta_Meas).
                Theta_Meas1 = delta_sum1
                
                # Update the proportional motor control 2, with the most recent measured theta and the setpoint
                Theta_Ref1 = 200
                    
                MoControl1.update(Theta_Meas1, Theta_Ref1)
                # Using the newly calculated actuation_signal from the encoder update
                # set the duty cycle
                actuation1 = MoControl1.Actuation_Signal
                moe1.set_duty(actuation1)
                    
                # Delay every loop for 10 msThis is place in order to allow the program to gradually
                # collect data at a more resonable rate, avoiding potential crashes.
    #            print('{}'.format(State))
                utime.sleep_ms(10)
            else:
                #  When the for loop is done, this will disable the motor and 
                #  proceed
                moe1.disable()
                
            #  Moving the Lever Arm DOWN
            for i in range (0, 100):
                #  Enable the motor driver
                moe1.enable()
                
                # Updates the encoder1 with the most recent position and calculates a new delta
                encoder1.update()
                    
                # Store the current value of delta as the new delta and add it to the running total of the deltas.
                new_delta1 = encoder1.position
                delta_sum1 += new_delta1
                    
                # Save the running total of the deltas as the distance traveled so far (Theta_Meas).
                Theta_Meas1 = delta_sum1
                
                # Update the proportional motor control 2, with the most recent measured theta and the setpoint
                Theta_Ref1 = 0
                    
                MoControl1.update(Theta_Meas1, Theta_Ref1)
                # Using the newly calculated actuation_signal from the encoder update
                # set the duty cycle
                actuation1 = MoControl1.Actuation_Signal
                moe1.set_duty(actuation1)
                    
                # Delay every loop for 10 msThis is place in order to allow the program to gradually
                # collect data at a more resonable rate, avoiding potential crashes.
    #            print('{}'.format(State))
                utime.sleep_ms(10)
            else:
                #  When the for loop is done, this will disable the motor and 
                #  proceed
                moe1.disable()
                
            #  Closing the Door
            for i in range (0, 100):
                #  Enable the motor driver
                moe2.enable()
                
                # Updates the encoder2 with the most recent position and calculates a new delta
                encoder2.update()
                    
                # Store the current value of delta as the new delta and add it to the running total of the deltas.
                new_delta2 = encoder2.position
                delta_sum2 += new_delta2
                    
                # Save the running total of the deltas as the distance traveled so far (Theta_Meas).
                Theta_Meas2 = delta_sum2   
            
                # Update the proportional motor control 2, with the most recent measured theta and the setpoint
                Theta_Ref2 = 200
                    
                MoControl2.update(Theta_Meas2, Theta_Ref2)
                # Using the newly calculated actuation_signal from the encoder update
                # set the duty cycle
                actuation2 = MoControl2.Actuation_Signal
                moe2.set_duty(actuation2)

                #  This is a delay that will allow the recording of the values to 
                #  be more gradual not performing this may cause the program to 
                #  crash due to the large amount of iterations it must perform. 
                utime.sleep_ms(10)

            else:
                #  When the for loop is done, this will disable the motor and 
                #  proceed
                moe2.disable()
        
        #  Condition for switch in the OFF position
        else:
            continue
        