## @file MotorControl.py
#  Brief doc for MotorControl.py
#
#  Detailed doc for MotorControl.py 
#
#  @author Jose Chavez
#
#  @copyright License Info
#
#  @date May 14, 2020
#
#  @package Motor Control
#  Brief doc for the MotorControl module
#
#  Detailed doc for the MotorControl module
#
#  @author Jose Chavez
#
#  @copyright License Info
#
#  @date May 14, 2020

## A motor control object
#
#  Details
#  @author Jose Chavez
#  @copyright License Info
#  @date May 14, 2020
import pyb
class CLPropCtrl:

    ## Constructor for the motor control
    #
    #  Detailed info on encoder driver constructor
    def __init__(self, Kp, Theta_Ref):
        ''' Creates a the closed loop proportional control.
        @param Kp Is the control gain determined by the user '''
        self.Kp = Kp
        self.Theta_Ref = Theta_Ref
   
   ##  Updates the recorded position of the encoder
    #
    #  Detailed info on encoder udpdate method
    def update(self, Theta_Meas, Theta_Ref):
        self.Theta_Ref = Theta_Ref
        self.Theta_Meas = Theta_Meas
    # Calculate the Error signal
        self.Error = self.Theta_Ref - self.Theta_Meas
        
    # Calculate the actuation signal
        self.Actuation_Signal = self.Error * self.Kp

if __name__ =='__main__':
    # Any code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create a control object passing in the pins and timer
    Theta_Ref = 1000
    Kp = 0.1
    MoControl = CLPropCtrl( Kp, Theta_Ref)