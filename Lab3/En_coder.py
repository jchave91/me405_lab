## @file En_coder.py
#  Brief doc for En_coder.py
#
#  The optical encoders used in this project are imbedded into the motors provided in the ME405 kits.
#  The encoders take advantage of the timers included with the STM32 processors.
#
#  @author Jose Chavez
#
#  @copyright License Info
#
#  @date May 5, 2020
#
#  @package encoder
#  The goal of the encoder driver is to measure where the encoder is at any particular time.
#
#  Detailed doc for the encoder module
#
#  @author Jose Chavez
#
#  @copyright License Info
#
#  @date May 5, 2020

## An encoder driver object
#
#  Details
#  @author Jose Chavez
#  @copyright License Info
#  @date May 5, 2020
import pyb
from micropython import const

offset = const(65536)      #The offset to correct the delta in case of an over/underflow
class Encoder:
    ## Constructor for encoder driver
    #
    #  Detailed info on encoder driver constructor
    def __init__(self, timerNUM, ENCA1_pin, ENCB1_pin, prescaler, period):
        ''' Creates the encoder by initializing the corresponding pins that
        encoder A and B are connected to. 
        @param ENCA1_pin A pyb.Pin object to use as the encoder A pin.
        @param ENCB1_pin A pyb.Pin object to use as the encoder B pin.
        @param timerNUM A pyb.Timer object to use as a counter. 
        @param prescaler A pyb.prescaler object
        @param period A pyb.period object'''
        #print ('Creating the encoder')
        # Import pyb in order for the REPL to recognize the commands
        #import pyb
        self.prescaler = prescaler
        self.period = period
        
        self.timer = pyb.Timer(timerNUM, prescaler = self.prescaler, period = self.period)

        self.timer.channel(1, self.timer.ENC_A, pin = ENCA1_pin)
        self.timer.channel(2, self.timer.ENC_B, pin = ENCB1_pin)
        
        # Initialize any previous conter variables to zero
        self.previous_count = 0
        self.position = 0
    ##  Updates the recorded position of the encoder
    #
    #  Detailed info on encoder udpdate method
    def update(self):
        ''' This function updates the encoder count and calculates a delta value. I will check
        for overflow and underflow occurrences and correct the delta accordingly. The corrected
        delta value will be added to the running total of encoder ticks.'''
    #  Get the current encoder ticks
        self.current_count = self.timer.counter()
    #Calculate a delta value by finding the difference between the current timer count
    #and the previous timer count 
        self.delta = self.current_count - self.previous_count
    # Check to see if the calculated delta is out of range. If the delta is larger than position
    # half the period (+32,767), an underflow has occured. Fix by offsetting delta by one whole number line
        if (self.delta > 32767):
            self.fixed_delta = self.delta - offset  # (offset = 65536)
            self.position += self.fixed_delta
    # Check for overflow and if there is, follow same procedure as for an underflow
        elif (self.delta < -32768):
            self.fixed_delta = self.delta + offset
            self.position += self.fixed_delta
    # If there is no over/underflow then do nothing       
        else:
            self.position += self.delta
    #Prepare for the next iteration by replacing the previous encoder count with the current one.
        self.previous_count = self.current_count
        
    ## Gets the encoder's position
    #
    #  This method is only meant to return the 
    def get_position(self):
        '''The purpose of this function is to return the most recently updated running total
        number of encoder ticks'''
        return self.position

        
    ## Sets the encoder positon
    #
    # Sets to a user specified value for the encoder position
    def set_position(self, count):
        '''This function sets the position to a desired value. The function will update
        the variables required in order to keep all other functions working properly.
        @param count Holds the value of the user desired position.'''
        self.previous_count = self.count
        self.position = self.count
#         self.previous_count = self.timer.counter() 
        
    ## Computes the difference between the last two encoder updates
    #
    # This method takes the difference from the old_update and the new_update.
    # It also check for any overflow or underflow occers and corrects it
    def get_delta(self):
        '''The purpose of this function is to return the difference in position between
        the most recent two updates'''
        return self.delta
        
    ## Zeros out the encoder
    #
    #  Detailed info on encoder zero function
    def zero(self):
        '''The purpose of this function is to set the position of the encoder to zero'''
        self.set_position(0)
        
if __name__ =='__main__':
    # Any code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    # Create the pin objects used for interfacing with the encoder
    
    # Sets the pins for encoder 1 as input pins
    ENCA1 = pyb.Pin (pyb.Pin.cpu.B6, pyb.Pin.IN)
    ENCB1 = pyb.Pin (pyb.Pin.cpu.B7, pyb.Pin.IN)
    
    # Sets the pins for encoder 2 as input pins
    ENCA2 = pyb.Pin (pyb.Pin.cpu.C6, pyb.Pin.IN)
    ENCB2 = pyb.Pin (pyb.Pin.cpu.C7, pyb.Pin.IN)
    
    # Encoder 1 works on timer 4
    Timer1 = 4
    
    # Encoder 2 works on timer 8
    Timer2 = 8
    
    # The period is preset to 65535
    period = 65535
    
    # The prescaler is preset to 0
    prescaler = 0
    
    # Encoder shortcut
    encoder1 = Encoder(Timer1, ENCA1, ENCB1, prescaler, period) 
    encoder2 = Encoder(Timer2, ENCA2, ENCB2, prescaler, period)
    