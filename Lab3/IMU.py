import pyb
from pyb import I2C
import ustruct
from micropython import const

bus = const(1)              #The bus number I am using
NDOF = const(0x0C)          #NDOF mode activates all 3 sensors
PWR = const(0x3E)           #Power mode- address within the slave
suspend = const(2)          #Pauses the system
BNO055 = const(0x28)        #Slave address
OPR = const(0x3D)           #OPR_MODE- address within the slave
ACC = const(0x01)           #This is the acceleration ID needed to get the values from that sensor
Calib_Stat = const(0x35)    #The address of the calibration status
Convertion = const(100)     # Used to convert acceleration data to m/s^2 (1 m/s^2 = 100 LSB AND 1 mg = 1 LSB) 
class IMU:
    #Initialize the IMU
    def __init__(self, i2c):
        self.i2c = i2c
    #Wakeup
    def  enable (self):
        i2c.mem_write(PWR, BNO055, 0) # Places the system in normal mode. All sensors are always switched ON in this mode.
        
    #Sleep
    def disable (self):
        i2c.mem_write(PWR, BNO055, 2) #Pauses the system and places all sensors and microcontroller into sleep mode        
    
    #sets the mode you want to operate in
    def set_mode(self):
        i2c.mem_write(NDOF, BNO055, OPR)  #Sets the operation mode to NDOF (This step fully enables the imu)
        
    #Provide the user with the current mode the IMU is in
    def get_mode(self):
        self.mode = i2c.mem_read(NDOF, BNO055, OPR)
        convert = ustruct.unpack('<b', self.mode)
        print('{}'.format(convert)) 


    def get_accel(self):
        ''' This function reads the acceleration values off of the sensor. The values are converted into readable numbers
        then into the units desired, which are m/s^2'''
        data = self.i2c.mem_read(6, BNO055, ACC)  #Read 6 bytes representing the LSB and MSB of the acceleration sensor
        x, y, z = ustruct.unpack('<hhh', data)    #Unpack the data into a 3-tuple [units are in mg]
        #Divide by the convertion factor to get the data in m/s^2
        x = x/Convertion
        y = y/Convertion
        z = z/Convertion
        return (x,y,z)
        
    #Provide the IMU's current calibration status
    def get_calibration(self):
        ''' This function will retrieve the calibration register which is a single byte of data. Each pair of bits within
        this register represents the calibration status for each sensor. The main purpose of this function is to isolate
        each pair and display them. The user will determine which pair to display depending on their interest. The pin
        descriptions are as follows: Pins <7:6> correspond to the system's calibration status, pins <5:4> correspond to the
        gyroscope's calibration status, pins <3:2> correspond to the acceleration's calibration status, and pins <1:0> correspond
        to the magnetometer's calibration status.
        All status display a range between 3 for fully calibrated and 0 for not calibrated. '''
        #Retrieve the calibration register located at 0x35
        data = self.i2c.mem_read(NDOF, BNO055, Calib_Stat)
        
        #Shift the data from the calibration register to the right as needed, then mask the result in order to only display the
        #bits desired.
        
        #Magnetometer calibration status
        self.mag_cal = (data[0] >> 0) & 0b11
        
        #Acceleration calibration status
        self.acc_cal = (data[0] >> 2) & 0b11
        #This will shift the register bits 4 and 5 to the right four times in order to get them into bits 0 and 1 (right alinged).
        #The result is then masked with 00000011 in order to clear all bits except the first two.
        #Gyroscope calibration status
        self.gyr_cal = (data[0] >> 4) & 0b11
        print('The magnetometer calibration status is: ', self.mag_cal,'\n' 'The acceleration calibration status is: ', self.acc_cal,'\n' 'The gyroscope calibration status is: ', self.gyr_cal)

if __name__ =='__main__':
    # Any code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    i2c = I2C(bus, I2C.MASTER)  #Create the I2C on bus 1 and init as a master  
    imu = IMU(i2c)              #Creating the IMU object and passing the I2C bus in to the constructor