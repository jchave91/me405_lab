import pyb
from pyb import I2C
import ustruct
from micropython import const

bus = const(1)              #The bus number I am using
NDOF = const(0x0C)          #NDOF mode activates all 3 sensors
PWR = const(0x3E)           #Power mode- address within the slave
suspend = const(2)          #Pauses the system
BNO055 = const(0x28)        #Slave address
OPR = const(0x3D)           #OPR_MODE- address within the slave
ACC = const(0x01)           # This is the acceleration ID needed to get the values from that sensor
class IMU:
    #Initialize the IMU
    def __init__(self, i2c):
        self.i2c = i2c
    #Wakeup
    def  enable (self):
        i2c.mem_write(PWR, BNO055, 0) # Places the system in normal mode. All sensors are always switched ON in this mode.
        
    #Sleep
    def disable (self):
        i2c.mem_write(PWR, BNO055, 2) #Pauses the system and places all sensors and microcontroller into sleep mode        
    
    #sets the mode you want to operate in
    def set_mode(self):
        i2c.mem_write(NDOF, BNO055, OPR)  #Sets the operation mode to NDOF (This step fully enables the imu)
        
    #Provide the user with the current mode the IMU is in
    def get_mode(self):
        self.mode = i2c.mem_read(NDOF, BNO055, OPR)
        convert = ustruct.unpack('<b', self.mode)
        print('{}'.format(convert)) 


    def get_accel(self):
        data = self.i2c.mem_read(6, BNO055, ACC)  # 0x01 is the ACC_ID
        #print('{}'.format(data)) #6 bytes representing LSB and MSB
        accel_values = ustruct.unpack('<hhh', data)
        print(accel_values)
        
    #Provide the IMU's current calibration status
    def get_calibration(self):
        pass
    
    #Gets the orientation as three Euler angles and returns them as a tuple
    def get_Orientation(self):
         pass   
if __name__ =='__main__':
    # Any code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    i2c = I2C(bus, I2C.MASTER)  #Create the I2C on bus 1 and init as a master  
    imu = IMU(i2c)              #Creating the IMU object and passing the I2C bus in to the constructor