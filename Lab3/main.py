## @file main.py
#  Brief doc for main.py
#
#  Detailed doc for main.py 
#
#  @author Jose Chavez
#
#  @copyright License Info
#
#  @date January 1, 1970
#
#
import pyb
import utime
import array
from Motor_Driver import MotorDriver
from En_coder import Encoder
from MotorControl import CLPropCtrl


if __name__ =='__main__':
    # Any code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    ## Motor Driver Initialization
    #    

    # Create the pin objects used for interfacing with the motor driver   
    # Sets pins of interest as output pins
    EN_pin  = pyb.Pin.cpu.A10
    pin_IN1 = pyb.Pin.cpu.B4
    pin_IN2 = pyb.Pin.cpu.B5 
    # Create the timer object used for PWM generation
    timer = pyb.Timer(3, freq = 20000);
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(EN_pin, pin_IN1, pin_IN2, timer)
    moe.enable()
    
    ## Encoder Initialization
    #
    #
    # Sets the pins for encoder 1 as input pins
    ENCA1 = pyb.Pin (pyb.Pin.cpu.B6, pyb.Pin.IN)
    ENCB1 = pyb.Pin (pyb.Pin.cpu.B7, pyb.Pin.IN)
    
    # Sets the pins for encoder 2 as input pins
    ENCA2 = pyb.Pin (pyb.Pin.cpu.C6, pyb.Pin.IN)
    ENCB2 = pyb.Pin (pyb.Pin.cpu.C7, pyb.Pin.IN)
    
    # Encoder 1 works on timer 4
    Timer1 = 4
    
    # Encoder 2 works on timer 8
    Timer2 = 8
    
    # The period is preset to 65535
    period = 65535
    
    # The prescaler is set to 0
    prescaler = 0
    
    # Encoder naming shortcut
    encoder1 = Encoder(Timer1, ENCA1, ENCB1, prescaler, period) 
    encoder2 = Encoder(Timer2, ENCA2, ENCB2, prescaler, period)
    
    ## Motor Proportion Control Initialization
    #
    #
    Theta_Ref = 0
    Kp = 0
    MoControl = CLPropCtrl( Kp, Theta_Ref)

    ## Main Program
    # Ask the user whether they want to generate a step resonse or not
    user_input = input('Would you like to generate a step response?(y)es or (n)o: ')
    # The user will be able to keep running the motor for 200 loops. The amount of loops were to chosen
    # to gather just enough data to generate the step reponse.
    while (True):           
        Theta_Meas = 0
        delta_sum = 0 
        if (user_input == 'y'):
            # The Try-Execpt contrust here is used to detect a non-numeric user input. If any detected
            # display an error message and  begin the loop again from the top.
            try:
                Kp = float(input('Please enter a positive gain value: '))
                # The gain has to be positive. Check for negative entries and reprompt for gain if any detected
                # If non are detected, do nothing and fall through to the next command
                if (Kp < 0):
                    print('The gain need to be a positive value. Try again: ')
                    continue
                else:
                    pass
                # Ask the user to enter the distance of travel desired.
                Theta_Ref = int(input('How far do you want to travel (Encoder Ticks)?' ))
                # The following two lines create a time based array and a positional based array.
                Tick_results = array.array('i', [])
                Position_results = array.array('i',[])
                # Run the motor for 200 passes. Just enough to visualize a quality response.
                for n in range(200):
                    # Updates the encoder with the most recent position and calculates a new delta
                    encoder1.update()
                    # Store the current value of delta as the new delta and add it to
                    # the running total of the deltas
                    new_delta = encoder1.delta
                    delta_sum = new_delta + delta_sum
                    # Save the running total of the deltas as the distance traveled so far (Theta_Meas).
                    Theta_Meas = delta_sum
                    # Append the values of time to the time based array
                    Tick_results.append(utime.ticks_ms())
                    # Append the values of position to the positional based array
                    Position_results.append(encoder1.latest_position)
                    # Update the proportional motor control with the most recent values
                    # of setpoint (Theta_ref) and distance traveled (Theta_Meas).
                    MoControl.update(Theta_Meas, Theta_Ref)
                    # Using the newly calculated actuation_signal from the encoder update
                    # set the duty cycle
                    moe.set_duty(MoControl.Actuation_Signal)
                    # Delay every loop for 10 msThis is place in order to allow the program to gradually
                    # collect data at a more resonable rate, avoiding potential crashes.
                    utime.sleep_ms(10)
                else:
                    encoder1.get_position()      
            except ValueError:           
                print('\n This is not a valid entry. Enter positive integers only.')
                continue
        #If the user's input is no then end the program         
        elif (user_input == 'n'):
             moe.disable()
             print('\n See you next time!')
             break   
        else:
             print('\n OOPS! not an option')
             user_input = input('Would you like to generate a step response?(y)es or (n)o: ')
             continue
        # If the loop was successful, prompt the user if they would like to generate anothe response
        # If they input no then the while loop will terminate, effectively ending the program.
        user_input = input('Would you like to try a different gain value?  (y)es \or (n)o?: ')
