#from pyb import I2C
import pyb


i2c = I2C(1, I2C.MASTER)             # create and init as a master

i2c.scan()       # Returns a list of addresses that are responding
# returns [40,118]    # 40 is the address to SCL and 118 is the address for SDA            

i2c.is_ready(40) # Checks to see if I2C device responds to that address
# returns TRUE
i2c.is_ready(40) # Checks to see if I2C device responds to that address
# returns TRUE

I2C.init(mode, *, addr=0x12, baudrate=400000, gencall=False, dma=False)
# Initialise the I2C bus with the given parameters:
# mode must be either I2C.MASTER or I2C.SLAVE
# addr is the 7-bit address (only sensible for a slave)
# baudrate is the SCL clock rate (only sensible for a master)
# gencall is whether to support general call mode
# dma is whether to allow the use of DMA for the I2C transfers (note that DMA transfers have more precise timing but currently do not handle bus errors properly)




I2C.mem_read(data, addr, memaddr, *, timeout=5000, addr_size=8)
# Read from the memory of an I2C device:
# data can be an integer (number of bytes to read) or a buffer to read into
# addr is the I2C device address
# memaddr is the memory location within the I2C device
# timeout is the timeout in milliseconds to wait for the read
# addr_size selects width of memaddr: 8 or 16 bits

I2C.mem_write(data, addr, memaddr, *, timeout=5000, addr_size=8)
# Write to the memory of an I2C device:
# data can be an integer or a buffer to write from
# addr is the I2C device address
# memaddr is the memory location within the I2C device
# timeout is the timeout in milliseconds to wait for the write
# addr_size selects width of memaddr: 8 or 16 bits
# Returns None. This is only valid in master mode.
# 

# # read from device
# 
#     def _read(self, buf, memaddr, addr):        # addr = I2C device address, memaddr = memory location within the I2C device
# 
#         '''
# 
#         Read bytes to pre-allocated buffer Caller traps OSError.
# 
#         '''
# 
#         self._mpu_i2c.readfrom_mem_into(addr, memaddr, buf)
# 
# 
# 
#     # write to device
# 
#     def _write(self, data, memaddr, addr):
# 
#         '''
# 
#         Perform a memory write. Caller should trap OSError.
# 
#         '''
# 
#         self.buf1[0] = data
# 
#         self._mpu_i2c.writeto_mem(addr, memaddr, self.buf1)
# 
# 
# i2c.is_ready(0x42)           # check if slave 0x42 is ready
# i2c.scan()                   # scan for slaves on the bus, returning
#                              #   a list of valid addresses
# i2c.mem_read(3, 0x42, 2)     # read 3 bytes from memory of slave 0x42,
#                              #   starting at address 2 in the slave

# i2c.mem_write('abc', 0x42, 2, timeout=1000)
    # write 'abc' (3 bytes) to memory of slave 0x42
    # starting at address 2 in the slave, timeout after 1 second
    
#UNIT SELECTION
#Acceleration m/s^2 [UNIT_SEL]: Register xxxxxxx0b
    
## LINEAR ACCEL CONVERTIONS
# 1 m/s^2 = 100 LSB AND 1 mg = 1 LSB

#     #Gets the orientation as three Euler angles and returns them as a tuple
#     def get_Orientation(self):
#          pass   