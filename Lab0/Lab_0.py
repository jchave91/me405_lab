''' @file Lab_0.py
This program asks the user to input an index and then the program will
calculates the Fibonacci number corresponding to that specific index.

Created on Mon Apr  6 15:20:41 2020

@author: josechavez

credit: (Did not use but it resembels my code so credit is given.)
<Saket Modi> (<date>) <Python Program for Fibonacci numbers>
 (<code version>) [<Sourve Code>].
 https://www.geeksforgeeks.org/python-program-for-program-for-fibonacci-numbers-2/

'''

def fib(n):
    #Defines the fuction
    ''' This function calculates the fibonacci number for any particular 
    index that the user inputs.
    @param n is an integer that specifies the index of the desired Fibonacci
    number. '''     
    
    if (n == 0):
        return 0
    #If the user's input was zero then return the value zero
    elif (n == 1):
        return 1
    #If the user's input was one then return the value one
    else:
        f = fib(int(n)-1) + fib(int(n)-2)
        return f
    #Otherwise calculate the Fibonacci number using the equation
    
if __name__ == "__main__":
    
    user_input = input('Would you like to find a Fibonacci number?  (y)es \
or (n)o?: ')
    #Ask user if he want to find a Fibonacci number  
    while (True):      
    #Keep running the program as long as the user's input is y         
        if (user_input == 'y'):
            try:
    #If the user says yes, then try the following.
                n = input('Please enter an index: ') 
                if n == '0':
                    print('\n The Fibonacci number associated with index value 0 is 0')
                
                elif n == '1':
                    print('\n The Fibonacci number associated with index value 1 is 1')   
                else:        
                    print('\n The Fibonacci Number associated with index',n,' is',fib(n))
            except ValueError:
    #If a non-integer value was input, through up an error message            
                print('\n This is not a valid entry. Enter positive integers only.')
        elif (user_input == 'n'):
            print('\n See you next time!')
            break
    #If the user's input is no then end the program    
        else:
            print('\n OOPS! not an option')
    #If the user input is anything other than y or n, through up an error message        
        user_input= input('Would you like to try again? (y)es or (n)o?: ')
    #Prompt user if he wants to continue. If yes, Loop continues and if no then it ends