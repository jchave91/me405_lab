## @file Motor_Driver.py
#  The MotorDriver.py is created to encapsulate all methods required to take advantage of all
#  the functionality that will be useful for interacting with the motor.
#
#  Detailed doc for MotorDriver.py 
#  
#    
#  @author Jose Chavez
#
#  @date April 29, 2020
## Latest Update
#  @date May 30, 2020
import pyb
class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''
    #import pyb
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin  A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer   A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        @param input_in Is the duty cycle determined by the user '''
        #print ('Creating a motor driver')
       
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
       
        self.EN_pin = pyb.Pin(self.EN_pin, pyb.Pin.OUT_PP)
        self.t3ch1 = timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        self.t3ch2 = timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
       ## Turns the motor on
    def enable (self):
        '''The purpose of this function is to enable the motor so it can be ready to take
        in a duty cycle that is defined by the user.'''
        
        self.EN_pin.high ()
        #print ('Enabling Motor')
        ## Turns the motor off
    def disable (self):
        '''The purpose of this function is to disable the motor. Once disabled, it will no
        longer accept any duty cycle.'''
        self.EN_pin.low ()
        #print ('Disabling Motor')
        ## Sets the motor's duty cycle
    def set_duty (self, duty):
        ''' This function sets a duty cycle that is then sent to the motor. Positive values
        cause the motor to spin in one direction and negative values in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor '''
        self.duty = duty
        if (self.duty > 0):
            self.t3ch2.pulse_width_percent(0)
            self.t3ch1.pulse_width_percent(self.duty)
            #print('Motor is spinning counter clock-wise')
        elif (self.duty == 0):
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(0)
        elif (self.duty < 0):
            self.duty = abs(self.duty)
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(self.duty)
            #print('Motor is spinning clock-wise')
        else:
            pass
        
if __name__ =='__main__':
    # Any code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    # Create the pin objects used for interfacing with the motor driver
    # Ask for user input to determine the duty cycle
    duty_in = int(input ('Enter the duty cycle desired (negative for ccw rotation):'))
    duty = duty_in
    
    # Sets pins of interest as output pins
    pin_EN  = pyb.Pin.cpu.A10
    pin_IN1 = pyb.Pin.cpu.B4
    pin_IN2 = pyb.Pin.cpu.B5 
    # Create the timer object used for PWM generation
    timer = pyb.Timer(3, freq = 20000);
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, timer)

    # Enable the motor driver
    moe.enable()

    # Set the duty cycle to 10 percent
    moe.set_duty(duty)
    
