## @file main.py
#  Brief doc for main.py
#
#  Detailed doc for main.py 
#
#  @author Jose Chavez
#
#  @copyright License Info
#
#  @date January 1, 1970
#
#

import MotorDriver

## A motor driver object
moto = MotorDriver.MotorDriver()

moto.set_duty_cycle(50)