## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  ME405 labs
#
#  @section sec_mot Motor Driver
#  The goal fo this lab is to write a Python class encapsulating all of the
#  functionality that will be useful for interacting with the motor and to
#  write test code evaluating the functionality of your motor driver class.
#
#  @section sec_enc Encoder Driver
#  Some information about the encoder driver with links. Please see encoder.Encoder which is part of the \ref encoder package.
#
#  @author Your name
#
#  @copyright License Info
#
#  @date January 1, 1970
#